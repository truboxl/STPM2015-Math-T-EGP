# STPM2015-Math-T-EGP

This repository contains some of my Excel spreadsheets that I created for use in my [STPM](https://en.wikipedia.org/wiki/Sijil_Tinggi_Persekolahan_Malaysia) 2015 Math T assignment to plot graphs automatically.

I am opening up for anyone (hopefully juniors) to download and learn the spreadsheets as examples if they are interested.

## What they do

Plot out equation graphs automatically (Semester 2)

Plot out 3 types of probability distribution in 1 same graph for comparisons under certain conditions (Semester 3)

## How to use

Excel will warn you some random error that the spreadsheet will not work properly. Don't worry about that. Just go ahead and enable Macro functionality and it will work.

Equation graphs spreadsheet should be self explanatory enough for you. Enter the equation and the graph comes out.

Probability distribution graphs spreadsheets need to specify few parameters to plot. Just check out the cells that have plain values and you can edit them.

If the spreadsheet you saved/made and you open them next time and it no longer works, please check the name labels again. Naughty Excel may have changed them, pointing to invalid referenced cells...

## Note

Instead of plotting graph based on rounded-up values from a table, these spreadsheets make use of an old Excel Macro functionality to plot graphs (charts) automatically.

This means that Excel will plot according to its own calculated values using specified functions by you, hence plotting a finer graph with automatic scaling. You can obtain the values directly from the graph.

However...

>["Excel 4 macros (XLM) that showed chart dialog boxes are no longer supported. XLM macros will still work in Office Excel 2007. However, we recommend that XLM macros are rewritten in Visual Basic for Applications (VBA)."](https://technet.microsoft.com/en-us/library/cc179167%28office.12%29.aspx#whatschanged5)

This means that **Macro-enabled spreadsheets (`.xlm` or `.xlsm`) will not work in Excel 2007** to plot the graph automatically.

Do not worry though because that functionality has returned in Excel 2010, 2013 and later versions due to popular demand.

## Help

If somebody out there willing to learn how to make one, you can refer to the comments I wrote inside the spreadsheets. However I must stress that **the comments may be wrong and only up to the point of my level of knowledge**. Therefore there will be a lot missing references and details that you may not understand.

If you need more help you can check out these references (and download their examples) that I used.
>http://www.jkp-ads.com/articles/ChartAnEquation00.asp

>http://www.oaltd.co.uk/DLCount/DLCount.asp?file=ChtFrmla.zip

>http://analyticsdemystified.com/excel-tips/excel-dynamic-named-ranges-with-tables-never-manually-updating-your-charts/

**Please also note that I have long forgotten most of the inner workings of these spreadsheets that I created in my school days! They are also provided as is! No warranties shall be claimed from me if they are not working!**

But you can try open up an issue to contact me. I will try to help you if I can although I no longer use Microsoft Office anymore...
